<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="styles/style.css">
  <title>Cost-calculator</title>
</head>
<body>



<script src="scripts/generationForm.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
 $('#btn').click(function () {

  let levels = $('#levels-prog').val();
  let typeWork = $('#type-work').val();
  let formatWork = $('#format-work').val();
  let hours = $('#hours').val();

  $.ajax({
    url: 'handler.php',
    type: 'POST',
    cache: false,
    data: {'levels' : levels, 'typeWork' : typeWork, 'formatWork' :
      formatWork, 'hours' : hours},
    dataType: 'html',
    success: function (data) {
      if (data == 'пустое поле') {
        $('#resultWindow').text('Введите количество часов');
        $('#hours').addClass('swing');
      } else {
        $('#resultWindow').text(data);
        $('#hours').removeClass('swing');

      }
    }


  });

 });

</script>
</body>
</html>