const generationForm = {
  //Шаблонное создание DOM елемента
  'createElement' : function (nameTag, className, atName_1, atValue_1, atName_2, atValue_2,
                              atName_3, atValue_3, atName_4, atValue_4) {
    let item = document.createElement(nameTag);
    item.classList.add(className);

    if (atName_1 && atValue_1) {
      item.setAttribute(atName_1, atValue_1);
    }
    if (atName_2 && atValue_2) {
      item.setAttribute(atName_2, atValue_2);
    }
    if (atName_3 && atValue_3) {
      item.setAttribute(atName_3, atValue_3);
    }
    if (atName_4 && atValue_4) {
      item.setAttribute(atName_4, atValue_4);
    }

    return item;
  },

  'createForm' : function (params) {
    let wrap = this.createElement('div', 'wrapper');
    document.body.appendChild(wrap);

    let form = this.createElement('form', 'form');
    wrap.appendChild(form)

    let title = this.createElement('h1', 'title');
    title.textContent = 'cost-calculator';
    form.appendChild(title);

    //генерация селекторов
    for (let i in params['selects']) {
      let id = i;
      let label = this.createElement('label', 'label', 'for', id);
      label.textContent = params['labels'][i];
      form.appendChild(label);
      let select = this.createElement('select', 'select', 'id', id);
      form.appendChild(select);
      for (let k in params['selects'][i]) {
        let option = this.createElement('option', 'option', 'value', params['selects'][i][k]);
        option.textContent = params['text'][i][k]
        select.appendChild(option);
      }
    };

    let label = this.createElement('label', 'label', 'for', 'hours');
    label.textContent = params['labels']['hours'];
    form.appendChild(label);

    let input = this.createElement('input', 'input', 'id', 'hours',
      'type','number');
    form.appendChild(input);

    let btn = this.createElement('input', 'btn', 'id', 'btn', 'type', 'button',
      'value', 'Расчитать стоимость работы');
    form.appendChild(btn);

    let result = this.createElement('p', 'p', 'id', 'resultWindow');
    form.appendChild(result);


    // Подстановка рандомных ценников к уровню программиста
    let juniorPrice = params['cost-of-work'].junior();
    let middlePrice = params['cost-of-work'].middle();
    let seniorPrice = params['cost-of-work'].senior();

    let junior = document.querySelector('option[value="Junior"]');
    junior.textContent += ' (' + juniorPrice + '$ за час работы)';
    junior.setAttribute('value', juniorPrice);

    let middle = document.querySelector('option[value="Middle"]');
    middle.textContent += ' (' + middlePrice + '$ за час работы)';
    middle.setAttribute('value', middlePrice);

    let senior = document.querySelector('option[value="Senior"]');
    senior.textContent += ' (' + seniorPrice + '$ за час работы)';
    senior.setAttribute('value', seniorPrice);


  }
};

let params = {
  'selects' : {
    'levels-prog': ['Junior', 'Middle', "Senior"],
    'type-work' : ['Subscriber', 'Piece'],
    'format-work' : ['Fast', 'Qualitatively']
  },
  'text' : {
    'levels-prog': ['Начинающий', 'Опытный', "Гуру"],
    'type-work' : ['Абоненская', 'Сдельная'],
    'format-work' : ['Быстро', 'Качественно']
  },
  'labels' : {
    'levels-prog' : 'Уровень программиста',
    'type-work' : 'Тип работы',
    'format-work' : 'Формат работы',
    'hours' : 'Часы работы программиста'},

  'cost-of-work' : {
    'junior' : function () {return Math.round(1 - 0.5 + Math.random() * (3 - 1 + 1))},
    'middle' : function () {return Math.round(4 - 0.5 + Math.random() * (8 - 4 + 1))},
    'senior' : function () {return Math.round(9 - 0.5 + Math.random() * (20 - 9 + 1))}
  },
  'inputs' : 'hours',
  'method' : 'post',
  'action' : 'handler.php',

};



generationForm.createForm(params);
